################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../WebServer/W5500/socket.c \
../WebServer/W5500/w5500.c \
../WebServer/W5500/wizchip_conf.c 

OBJS += \
./WebServer/W5500/socket.o \
./WebServer/W5500/w5500.o \
./WebServer/W5500/wizchip_conf.o 

C_DEPS += \
./WebServer/W5500/socket.d \
./WebServer/W5500/w5500.d \
./WebServer/W5500/wizchip_conf.d 


# Each subdirectory must supply rules for building sources it contributes
WebServer/W5500/%.o: ../WebServer/W5500/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\Noise_sensor\bluepill" -I"H:\STM32ToolChain\Workspace\Noise_sensor\NoiseSensor" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WS2813Strip" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer\W5500" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


