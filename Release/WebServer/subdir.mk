################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../WebServer/WebServer.cpp 

OBJS += \
./WebServer/WebServer.o 

CPP_DEPS += \
./WebServer/WebServer.d 


# Each subdirectory must supply rules for building sources it contributes
WebServer/%.o: ../WebServer/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\Noise_sensor\bluepill" -I"H:\STM32ToolChain\Workspace\Noise_sensor\NoiseSensor" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WS2813Strip" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer\W5500" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


