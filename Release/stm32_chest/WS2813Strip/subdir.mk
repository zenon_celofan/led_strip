################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../stm32_chest/WS2813Strip/WS2813Strip.cpp 

OBJS += \
./stm32_chest/WS2813Strip/WS2813Strip.o 

CPP_DEPS += \
./stm32_chest/WS2813Strip/WS2813Strip.d 


# Each subdirectory must supply rules for building sources it contributes
stm32_chest/WS2813Strip/%.o: ../stm32_chest/WS2813Strip/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\LED_strip\bluepill" -I"H:\STM32ToolChain\Workspace\LED_strip\stm32_chest\WS2813Strip" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


