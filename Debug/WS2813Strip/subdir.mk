################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../WS2813Strip/WS2813Strip.cpp 

OBJS += \
./WS2813Strip/WS2813Strip.o 

CPP_DEPS += \
./WS2813Strip/WS2813Strip.d 


# Each subdirectory must supply rules for building sources it contributes
WS2813Strip/%.o: ../WS2813Strip/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\Noise_sensor\bluepill" -I"H:\STM32ToolChain\Workspace\Noise_sensor\NoiseSensor" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WS2813Strip" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer" -I"H:\STM32ToolChain\Workspace\Noise_sensor\WebServer\W5500" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


