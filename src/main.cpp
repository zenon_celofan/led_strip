#include <stdio.h>
#include <string.h>

#include "bluepill.h"
#include "Led.h"
#include "Serial.h"
#include "millis.h"
#include "WS2813Strip.h"


#define NUMBER_OF_LEDS_IN_STRIP		30

Led 		board_led(PC13, ACTIVE_LOW);
Serial		serial3(USART3);

WS2813Strip	led_strip(NUMBER_OF_LEDS_IN_STRIP);
uint32_t 	array_with_colors_for_each_diode[NUMBER_OF_LEDS_IN_STRIP];// = {RED, 0, YELLOW, 0, BLUE};
const uint32_t	constant_colors[NUMBER_OF_LEDS_IN_STRIP] = {0,	GREEN,	YELLOW, 0, 0,  0, 	0, 	0, 	0, 	0,
															0,	0,	0,	0, 	0,  0, 	0, 	RED, 	0, 	0,
															0,	0,	0,	0, 	0,  BLUE, 	0, 	0, 	0, 	0};


inline void clear_led_strip(void);

void main() {

	millis_init();
	//adc_random_init();

	serial3.send("\n\n\nRESET\n");

	delay(100);
	led_strip.clear();
	delay(100);

    while(1) {

    	clear_led_strip();

    	for (uint8_t m = 0; m < 3; ++m) {
        	uint8_t random_led;
        	uint32_t color;

    		random_led = random(10) + (m * 10);
    		if (constant_colors[random_led] != 0) {
    			color = constant_colors[random_led];
    		} else {
    			color = WHITE;
    		}

    		array_with_colors_for_each_diode[random_led] = color;
    	}

    	//array_with_colors_for_each_diode[1] = RED;

    	led_strip.led_strip_send(array_with_colors_for_each_diode);

    	board_led.toggle();
    	delay(500);
    }
} //main


inline void clear_led_strip(void) {
	for (int i = 0; i < NUMBER_OF_LEDS_IN_STRIP; ++i) {
		array_with_colors_for_each_diode[i] = OFF;
	}
	//led_strip.led_strip_send(array_with_colors_for_each_diode);
}
